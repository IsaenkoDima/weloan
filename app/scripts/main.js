var app = {

  init: function () {
    app.windowResize();
    app.fullScreenHeight();
    app.menu();
    app.loan();
    app.custom();
    app.inputdecor();
    app.UIslider();
    app.modals();
    app.chartjs();
    app.selectric();
    app.tabs();
    app.validate();
    app.accordeon();
    app.table();
  },

  windowResize: function () {
    $(window).on('resize', function () { });
  },

  windowLoad: function () {
    $(window).on('load', function () { });
  },


  loan: function () {
    $('.jsLoanRadio').each(function () {
      var $this = $(this),
        $radio = $this.find('input[type="radio"]');

      $radio.each(function () {
        $(this).on('change', function () {
          $('.form_btn').addClass('valid').find('[type="submit"]').prop('disabled', false);
          $('.what_purpose').slideUp(300);
          var $link = $(this).attr('data-link');
          $('#loan_link').attr('href', $link);
          console.log($link);
        });
      });

      $('#radio_other').on('change', function () {
        if ($(this).is(':checked')) {
          $('.what_purpose').slideDown(300);
        }
      });
    });
  },


  custom: function () {
    $(window).on('load', function () {
      if($('#loading_redirect').length){
        setTimeout(function(){
          $(location).attr('href', '/loan_offer_results.html')
        }, 4000);
      }
    });


    
    $('.b_offer_results .results_table .button_checkbox').each(function(){
      var $this = $(this);
      var allcheckbox = $this.find('[type="checkbox"]');
      
      allcheckbox.on('change', function(){
        var legchecked = $('.b_offer_results .results_table [type="checkbox"]:checked').length;

        if(legchecked){
          $('.selected_offers_wrapper').fadeIn(300);
          $('.selected_offers_wrapper .selected_item').text(legchecked)
        } else {
          $('.selected_offers_wrapper').fadeOut(300);
          $('.selected_offers_wrapper .selected_item').text(legchecked)
        }
        
      });
    });


    $('.b_header').sticky({
      topSpacing: 0,
      zIndex: 5
    });


    $('.jsChooseDate').each(function () {
      var $this = $(this);
      $this.datepicker({
        position: 'top right',
        language: 'en'
      });
    });


    $('#modal_resultes .button_checkbox').each(function(){
      var $this = $(this);
      var allcheckbox  = $this.find('[type="checkbox"]');
      
      allcheckbox.on('change', function(){
        var legchecked = $('#modal_resultes [type="checkbox"]:checked').length;

        if(legchecked){
          $('#modal_resultes .table_wrapper').addClass('selected');
          $('.selected_offers_wrapper').fadeIn(300);
        } else {
          $('#modal_resultes .table_wrapper').removeClass('selected');
          $('.selected_offers_wrapper').fadeOut(300);
        }
        
      });
    });



    $('.jsBtnHideHead').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $('.b_single_company').toggleClass('hide_company_actions');

      if ($(window).width() > 1200) {
        $('.jsLoansWrapper').mCustomScrollbar('destroy').mCustomScrollbar({
          scrollInertia: 200,
          axis: 'yx',
          advanced: { autoExpandHorizontalScroll: true },
          // setLeft: 0
        });
      }
    });


    $('.btn_toggle_main_data_body').on('click', function () {
      $(this).toggleClass('active');
      $('.main_data_body').slideToggle(300);
    });

    $('.drop_menu_wrapper').each(function () {
      var $this = $(this),
        $thisBtn = $this.find('.jsDropMenuBtn'),
        $thisMenu = $this.find('.jsDropMenu');

      $thisBtn.on('click', function () {
        $thisMenu.fadeIn(200);
      });
      closeMenu($thisBtn, $thisMenu);
    });
    function closeMenu($thisBtn, $thisMenu) {
      $(document).on('click', function (e) {
        if (!$thisBtn.is(e.target) && $thisBtn.has(e.target).length === 0 &&
          !$thisMenu.is(e.target) && $thisMenu.has(e.target).length === 0) {
          $thisMenu.fadeOut(200);
        }
      });
    }




    $('.input_password').each(function () {
      var $this = $(this);
      $this.on('blur keyup keypress', function () {
        if (!$this.val()) {
          $this.removeClass('not_empty');
        } else {
          $this.addClass('not_empty');
        }
      });
    });


    $('.jsCustomScrollbar').each(function () {
      var $this = $(this);
      $this.mCustomScrollbar({
        scrollInertia: 200
      });
    });

    $(window).on('load', function () {
      if ($(window).width() > 768) {
        $('.jsLoansWrapper').each(function () {
          var $this = $(this);
          $this.mCustomScrollbar({
            scrollInertia: 200,
            axis: 'yx'
          });
        });
      }
    });



    $('.jsCompanyActionsBbody').each(function () {
      var $this = $(this);
      $this.mCustomScrollbar({
        scrollInertia: 200
      });

      $(window).on('load resize', function () {
        if ($(window).width() < 768) {
          $this.mCustomScrollbar('destroy');
        }
      });
    });
    $('.jsCompanyInformation').each(function () {
      var $this = $(this);
      $this.mCustomScrollbar({
        scrollInertia: 200
      });

      $(window).on('load resize', function () {
        if ($(window).width() < 768) {
          $this.mCustomScrollbar('destroy');
        }
      });
    });



    $('.jsAddCompany').on('click', function (e) {
      e.preventDefault();
      $('#add_company').fadeIn(300).addClass('active');
      $('html, body').addClass('overflow');
    });
    $('.jsCloseWrapperAdd').on('click', function (e) {
      e.preventDefault();
      $(this).parents('.b_add_wrapper').removeClass('active').fadeOut(300);
      $('html, body').removeClass('overflow');
    });


    $('.jsAddNote').on('click', function (e) {
      e.preventDefault();
      $('.new_note_form').fadeIn(300);
      $('html, body').addClass('overflow');
    });
    $('.jsAddTask').on('click', function (e) {
      e.preventDefault();
      $('.new_task_form').fadeIn(300);
      $('html, body').addClass('overflow');
    });
    $('.jsCloseNoteForm').on('click', function (e) {
      e.preventDefault();
      $(this).parents('.custom_form_wrapper').fadeOut(300);
      $('html, body').removeClass('overflow');
    });


    $('.jsCreatingLoan').on('click', function (e) {
      e.preventDefault();
      $('#creating_loan').fadeIn(300).addClass('active');
    });


    $(document).click(function (e) {
      var btnCreatingLoan = $('.jsCreatingLoan');
      var btnAddCompany = $('.jsAddCompany'),
        addWrapper = $('.b_add_wrapper .add_wrapper');

      if (!btnAddCompany.is(e.target) && btnAddCompany.has(e.target).length === 0 &&
        !addWrapper.is(e.target) && addWrapper.has(e.target).length === 0) {
        $('#add_company').fadeOut(300).removeClass('active');
      }

      if (!btnCreatingLoan.is(e.target) && btnCreatingLoan.has(e.target).length === 0 &&
        !addWrapper.is(e.target) && addWrapper.has(e.target).length === 0) {
        $('#creating_loan').fadeOut(300).removeClass('active');
      }
    });

    $(window).on('load', function () {
      $('.b_team .team_item').each(function () {
        var $this = $(this),
          $thisItemTop = $this.find('.item_top').innerHeight(),
          $thisHeight = $this.find('.item_content').innerHeight();
        var content = $this.find('.content_inner'),
          contentHeight = content.innerHeight();
        var fullHeight = $thisItemTop + contentHeight;

        $this.mouseover(function () {
          if (contentHeight > $thisHeight) {
            $this.find('.box_decor').height(fullHeight);
            content.parents('.content_wrapper').stop(true, false).height(contentHeight).addClass('open');
          }
        }).mouseleave(function () {
          $this.find('.box_decor').height('100%');
          content.parents('.content_wrapper').stop(true, false).height('100%').removeClass('open');
        });
      });
    });
  },


  inputdecor: function () {
    $('.jsMaskFormControl .form_control').each(function (e) {
      $(this).wrap('<fieldset></fieldset>');
      var tag = $(this).attr('placeholder');
      $(this).attr('placeholder', '');
      $(this).after('<span class="label_top">' + tag + '</span>');
    });

    $('fieldset').each(function () {
      if ($(this).children('input').length > 0) {
        $(this).addClass('fieldset_input');
        // $(this).append('<span class="jsClearInput"></span>'); 
      }
      if ($(this).children('textarea').length > 0) {
        $(this).addClass('fieldset_textarea');
      }
    });

    function checkInput() {
      $('.jsMaskFormControl .form_control').each(function () {
        if (!$(this).val() == '') {
          $(this).parents('fieldset').addClass('has_text').find('.label_top').addClass('stay');
        } else {
          $(this).parents('fieldset').removeClass('has_text').find('.label_top').removeClass('stay');
        }
      });

    }

    checkInput();

    $('.jsMaskFormControl .form_control').on('change blur', function () {
      if (!$(this).val() == '') {
        $(this).parents('fieldset').addClass('has_text').find('.label_top').addClass('stay');
      } else {
        $(this).parents('fieldset').removeClass('has_text').find('.label_top').removeClass('stay');
      }
    });
  },


  UIslider: function () {
    var $loanamount = $('#loanamount');
    var instance;
    var $input = $('#irs-input-value');
    var $inputmobile = $('#irs-input-value-mobile');
    var $min = parseFloat($loanamount.attr('data-min'));
    var $max = parseFloat($loanamount.attr('data-max'));

    $loanamount.ionRangeSlider({
      grid: false,
      grid_marginl: false,
      min: $min,
      max: $max,
      step: 1000,
      prettify_separator: ',',
      prefix: '₪ ',
      onStart: function (data) {
        setTimeout(function () {
          var irsSingle = $('.irs-single').not('.irs-single.irs-single-custom').attr('style');
          $('.irs-single-custom').attr('style', irsSingle);
          var irsSingleText = $('.irs-single').not('.irs-single.irs-single-custom').text();
          $('.irs-single-custom .irs-data-single').text(irsSingleText);
          $input.prop('value', data.from);
          $inputmobile.prop('value', data.from);

          var v = data.from;
          function numberWithCommas(v) {
            return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          }
          $('#loanamountdata').text(numberWithCommas(v));
        }, 100);
      },
      onChange: function (data) {
        var irsSingle = $('.irs-single').not('.irs-single.irs-single-custom').attr('style');
        var irsSingleText = $('.irs-single').not('.irs-single.irs-single-custom').text();
        $('.irs-single-custom .irs-data-single').text(irsSingleText);
        $('.irs-single-custom').attr('style', irsSingle);
        $input.prop('value', data.from);
        $inputmobile.prop('value', data.from);
      },
    });

    instance = $loanamount.data('ionRangeSlider');
    
    
    $(document).on('click', '.irs-data-done', function () {
      var val = $(this).parents('.irs-single-custom').find('input').prop('value');
      
      if (val < $min) {
        val = $min;
      } else if (val > $max) {
        val = $max;
      }
      
      instance.update({
        from: val
      });
      
      var irsSingle = $('.irs-single').not('.irs-single.irs-single-custom').attr('style');
      $('.irs-single-custom').attr('style', irsSingle);
      var irsSingleText = $('.irs-single').not('.irs-single.irs-single-custom').text();
      $('.irs-single-custom .irs-data-single').text(irsSingleText);
    });

    

    function irsValueMobile(){
      var val = $('#irs-input-value-mobile').prop('value');
      if (val < $min) {
        val = $min;
      } else if (val > $max) {
        val = $max;
      }
      
      instance.update({
        from: val
      });
      var irsSingle = $('.irs-single').not('.irs-single.irs-single-custom').attr('style');
      $('.irs-single-custom').attr('style', irsSingle);
      var irsSingleText = $('.irs-single').not('.irs-single.irs-single-custom').text();
      $('.irs-single-custom .irs-data-single').text(irsSingleText);
    }

    $(document).on('click', '.irs-data-done-mobile', function () {
      irsValueMobile();
    });

    $('#irs-input-value-mobile').focusout(function(){
      irsValueMobile();
      $('.irs-data-mobile-wrapper').fadeOut(200);
    });

    $loanamount.on('change', function () {
      var $inp = $(this);
      var v = $inp.prop('value');
      function numberWithCommas(v) {
        return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      }
      $('#loanamountdata').text(numberWithCommas(v));
      // console.log(v, from, to);
    });

    $(document).on('click', '.irs-data-button', function(){
      // $('.custom_value_wrapper').slideToggle(300);
      $(this).hide();
      $('.irs-data-done').fadeIn(300);
      $('#irs-input-value').fadeIn(300).focus();
    });

    $(document).on('click', '.irs-data-done', function(){
      // $('.custom_value_wrapper').slideToggle(300);
      $(this).hide();
      $('.irs-data-button').fadeIn(300);
      $('#irs-input-value').fadeOut(300);
    });

    $(document).on('click', '.irs-data-button-mobile', function(){
      $('.irs-data-mobile-wrapper').fadeIn(300);
    });

    $(document).on('click', '.irs-data-done-mobile', function(){
      $('.irs-data-mobile-wrapper').fadeOut(200);
    });



    $('.jsMinus').click(function () {
      var $input = $(this).parent().find('input');
      if ($input.val() == '') {
        $input.val('1');
      }
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
    });
    $('.jsPlus').click(function () {
      var $input = $(this).parent().find('input');
      if ($input.val() == '') {
        $input.val('1');
      }
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    });
    $('.input_count .input_value').on('change', function () {
      var count = $(this).val();
      $(this).parent().find('.jsInputValue').text(count);
    });
  },


  fullScreenHeight: function () {
    $(window).on('resize load', function () {
      var headHeight = 0,
        footHeight = 0;

      if ($('header').length) {
        headHeight = $('header').outerHeight();
      }
      if ($('footer').length) {
        footHeight = $('footer').outerHeight();
      }

      const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

      $('main').css('min-height', height - headHeight - footHeight);
      $('.jsDeviceHeight').css('height', height);
      $('.jsCustomHeight').css('min-height', height);
      $('.jsHeightWithoutHeader').css('height', height - headHeight);
      $('.jsHeightWithoutHeaderLoan').css('min-height', height - headHeight);
    });
  },


  menu: function () {
    var $btnMenu = $('.jsMenu');
    $btnMenu.click(function () {
      $(this).toggleClass('menu-is-active');
      $('.header_menu').fadeToggle(300);
      $('body').toggleClass('menuopen');
    });

    $('.jsDashboardMenu').on('click', function (e) {
      e.preventDefault();
      $('.b_dashboard_mobile_menu').fadeIn(300);
      $('body').addClass('mobile_menu_open');
    });

    $(document).click(function (e) {
      var DashboardMenu = $('.jsDashboardMenu');
      var MobileMenuInner = $('.dashboard_mobile_menu_inner');
      if (!DashboardMenu.is(e.target) && DashboardMenu.has(e.target).length === 0 &&
        !MobileMenuInner.is(e.target) && MobileMenuInner.has(e.target).length === 0) {
        $('.b_dashboard_mobile_menu').fadeOut(300);
        $('body').removeClass('mobile_menu_open');
      }
    });



    $('.jsPersonalMenu').on('click', function () {
      $('.personal_menu').slideToggle(300);
    });
  },


  selectric: function () {
    $('.jsSelectricView').selectric({
      maxHeight: 150,
      disableOnMobile: false,
      nativeOnMobile: false
    });
  },


  modals: function () {
    $('.jsOpenModals').magnificPopup({
      removalDelay: 300,
      mainClass: 'my-mfp-slide-bottom'
    });

    $('.jsModalResult').magnificPopup({
      removalDelay: 300,
      mainClass: 'my-mfp-slide-bottom',
      closeOnBgClick: false,
      enableEscapeKey: false
    });

    $('.jsAddSearchBar').magnificPopup({
      mainClass: 'my-mfp-slide-bottom',
      items: {
        src: '#add_search',
        type: 'inline'
      }
    });
  },


  tabs: function () {

    var tabs = $('.jsTabs');
    tabs.each(function () {

      var tabs = $(this),
        tab = tabs.find('.jsTabsTab'),
        content = tabs.find('.jsTabsItem');

      tab.each(function (index, element) {
        $(this).attr('data-tab', index);
      });

      function showContent(i) {
        tab.removeClass('-active');
        content.removeClass('-active').removeClass('-fade');
        tab.eq(i).addClass('-active');
        content.eq(i).addClass('-active');
        setTimeout(function () {
          content.eq(i).addClass('-fade');
        }, 1);
      }

      tab.on('click', function (e) {
        e.preventDefault();
        showContent(parseInt($(this).attr('data-tab')));
      });

    });
  },


  chartjs: function () {

    if ($('#requests_sent').length) {
      var ctx = document.getElementById('requests_sent');
      var ctx = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ['1/08', '5/08', '10/08', '15/08', '20/08', '25/08'],
          datasets: [{
            borderWidth: 0,
            fill: true,
            backgroundColor: '#0597F2',
            data: [100, 30, 50, 15, 10, 40]
          }],
        },
        options: {

          responsive: true,
          tooltips: {
            intersect: false
          },
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0, 0, 0, 0)',
              }
            }],
            yAxes: [{
              ticks: {
                beginAtZero: true
              },
              gridLines: {
                color: 'rgba(0, 0, 0, 0)',
              }
            }]
          },
          elements: {
            point: {
              radius: 0
            }
          },
          legend: {
            display: false,
          },
          title: {
            display: false,
            text: ''
          },
          animation: {
            animateScale: true,
            animateRotate: true
          }
        }
      });
    }

    if ($('#loans_snapshot').length) {
      var ctx = document.getElementById('loans_snapshot');
      var ctx = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['בהמתנה', 'בחיתום', 'בטיפול', 'המתנה לוועדה', 'מאושר'],
          datasets: [{
            label: '# of Votes',
            data: [45, 15, 15, 15, 10],
            backgroundColor: [
              '#438EC4',
              '#4A56DA',
              '#CE4ADA',
              '#864ADA',
              '#DA4A9E',
            ],
            borderWidth: 0
          }]
        },
        options: {
          cutoutPercentage: 70,
          responsive: true,
          legend: {
            position: 'right',
            rtl: true,
            labels: {
              boxWidth: 16,
              fontSize: 16
            }
          },
          title: {
            display: false,
            text: ''
          },
          animation: {
            animateScale: true,
            animateRotate: true
          }
        }
      });
    }

    if ($('#source_requests').length) {
      var ctx = document.getElementById('source_requests');
      var ctx = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['בהמתנה', 'בחיתום', 'בטיפול', 'המתנה לוועדה', 'מאושר'],
          datasets: [{
            label: '# of Votes',
            data: [45, 15, 15, 15, 10],
            backgroundColor: [
              '#438EC4',
              '#4A56DA',
              '#CE4ADA',
              '#864ADA',
              '#DA4A9E',
            ],
            borderWidth: 0
          }]
        },
        options: {
          cutoutPercentage: 70,
          responsive: true,
          legend: {
            position: 'right',
            rtl: true,
            labels: {
              boxWidth: 16,
              fontSize: 16
            }
          },
          title: {
            display: false,
            text: ''
          },
          animation: {
            animateScale: true,
            animateRotate: true
          }
        }
      });
    }

  },


  validate: function () {

  },


  accordeon: function () {
    $('.jsAccord').each(function () {
      var accord = $(this),
        accord_btn = accord.find('.jsAccordBtn'),
        accord_content = accord.find('.jsAccordContent'),
        accord_item = accord.find('.jsAccordItem');

      accord_btn.on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
          $this_item = $this.closest('.jsAccordItem'),
          $this_content = $this.closest('.jsAccordItem').find('.jsAccordContent');
        if ($this.hasClass('-active')) {
          $this.removeClass('-active');
          $this_content.slideUp();
          $this_item.removeClass('item_active');
        } else {
          accord_item.removeClass('item_active');
          accord_btn.removeClass('-active');
          accord_content.slideUp();
          $this.addClass('-active');
          $this_content.slideDown();
          $this_item.addClass('item_active');
        }
      });
    });
  },


  table: function () {
    $('.jsTableTransformMobile').each(function () {
      var $this = $(this);
      $this.find('thead tr th').each(function () {
        var el = $(this);
        $this.find('tbody tr').each(function () {
          $(this).find('td').eq(el.index()).attr('data-name', el.text());
        });
      });

    });
  },

};

$(document).ready(app.init());

app.windowLoad();
